// Fill out your copyright notice in the Description page of Project Settings.


#include "TankTrack.h"
#include "SpawnPoint.h"
#include "SprungWheel.h"

void UTankTrack::SetThrottle(float Throttle)
{
    float ForceApplied = MaxTrackDrivingForce * Throttle;
    TArray<ASprungWheel*> SprungWheels = GetSprungWheels();
    float ForcePerWheel = ForceApplied/SprungWheels.Num();
    for(auto SprungWheel : SprungWheels)
    {
        SprungWheel->AddWheelDrivingForce(ForcePerWheel);
    };
}

TArray<USpawnPoint*> UTankTrack::GetAttachedSpawnPoints() const
{
    TArray<USpawnPoint*> SpawnPoints;
    TArray<USceneComponent*> Children; 
    GetChildrenComponents(true, Children);
    for(auto Child : Children)
    {
        USpawnPoint* SpawnPointChild = Cast<USpawnPoint>(Child);
        if(!SpawnPointChild) {continue;};
        SpawnPoints.Add(SpawnPointChild);
    }
    return SpawnPoints;
}

TArray<ASprungWheel*> UTankTrack::GetSprungWheels() const
{
    TArray<USpawnPoint*> SpawnPoints = GetAttachedSpawnPoints();
    TArray<ASprungWheel*> SprungWheels;
    for(auto SpawnPoint : SpawnPoints)
    {
        if(!SpawnPoint) {continue;};
        ASprungWheel* SprungWheel = Cast<ASprungWheel>(SpawnPoint->GetSpawnedActor());
        if(!SprungWheel) {continue;};
        SprungWheels.Add(SprungWheel);
    }
    return SprungWheels;
}