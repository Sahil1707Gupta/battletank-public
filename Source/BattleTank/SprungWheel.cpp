// Fill out your copyright notice in the Description page of Project Settings.


#include "SprungWheel.h"

// Sets default values
ASprungWheel::ASprungWheel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostPhysics;
	SpringConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("SpringConstraint"));
	SetRootComponent(SpringConstraint);

	Axle = CreateDefaultSubobject<USphereComponent>(FName("Axle"));
	Axle->SetupAttachment(SpringConstraint);

	AxleWheelConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("AxleWheelConstraint"));
	AxleWheelConstraint->SetupAttachment(Axle);

	Wheel = CreateDefaultSubobject<USphereComponent>(FName("Wheel"));
	Wheel->SetupAttachment(Axle);
}

// Called when the game starts or when spawned
void ASprungWheel::BeginPlay()
{
	Super::BeginPlay();
	Wheel->SetNotifyRigidBodyCollision(true);
	Wheel->OnComponentHit.AddDynamic(this, &ASprungWheel::OnHit);
	SetupSpringConstraint();
	SetupAxleWheelConstraint();
}

void ASprungWheel::SetupSpringConstraint()
{
	if(!GetAttachParentActor()) {return;};
	SpringConstraint->SetConstrainedComponents
	(
		Cast<UPrimitiveComponent>(Axle),
		NAME_None,
		Cast<UPrimitiveComponent>(GetAttachParentActor()->GetRootComponent()),
		NAME_None
	);
}

void ASprungWheel::SetupAxleWheelConstraint()
{
	AxleWheelConstraint->SetConstrainedComponents
	(
		Cast<UPrimitiveComponent>(Axle),
		NAME_None,
		Cast<UPrimitiveComponent>(Wheel),
		NAME_None
	);
}

void ASprungWheel::AddWheelDrivingForce(float Force)
{
	TotalForceMagnitudeThisFrame += Force;
}

// Called every frame
void ASprungWheel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(GetWorld()->TickGroup == TG_PostPhysics)
	{
		TotalForceMagnitudeThisFrame = 0;
	};
}

void ASprungWheel::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ApplyForce();
}

void ASprungWheel::ApplyForce()
{
	FVector ForceApplied = Axle->GetForwardVector() * TotalForceMagnitudeThisFrame;
	Wheel->AddForce(ForceApplied);
}



