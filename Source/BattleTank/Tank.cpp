// Fill out your copyright notice in the Description page of Project Settings.

#include "Tank.h"
#include "TankAimingComponent.h"
#include "TankBarrel.h"
#include "Projectile.h"

// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	// No need to protect pointers in constructors.....
	TankAimingComponent = CreateDefaultSubobject<UTankAimingComponent>(FName("AimingComponent"));
}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();
}

// Called to bind functionality to input
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ATank::AimAt(const FVector& HitLocation) const
{
	if(TankAimingComponent)
	{
		TankAimingComponent->AimAt(HitLocation, LaunchSpeed);
	};
}

void ATank::SetBarrelReference(UTankBarrel* BarrelToSet)
{
	if(TankAimingComponent)
	{
		TankAimingComponent->SetBarrelReference(BarrelToSet);
	};

	Barrel = BarrelToSet;
}

void ATank::SetTurretReference(UTankTurret* TurretToSet)
{
	if(TankAimingComponent)
	{
		TankAimingComponent->SetTurretReference(TurretToSet);
	}
}

void ATank::Fire()
{
	bool IsReloaded;

	if(LastFireTime == 0)
	{
		IsReloaded = true;
	}
	else
	{
		IsReloaded = (GetWorld()->GetTimeSeconds() - LastFireTime) > ReloadTimeInSeconds;	
	}

	if(Barrel && IsReloaded)
	{
		AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(
											ProjectileBlueprint,
											Barrel->GetSocketLocation(FName("Projectile")),
											Barrel->GetSocketRotation(FName("Projectile"))
										);
		if(Projectile)
		{
			Projectile->Launch(LaunchSpeed);
		};

		LastFireTime = GetWorld()->GetTimeSeconds();
	};
}