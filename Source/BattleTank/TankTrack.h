// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

class USpawnPoint;
class ASprungWheel;

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	// Sets a throttle between -1 and +1
	UFUNCTION(BlueprintCallable, Category = Input)
	void SetThrottle(float Throttle);

	UPROPERTY(EditDefaultsOnly, Category = Movement)
	float MaxTrackDrivingForce = 616900.f; // for tank of mass 61690kg and acceleration 10 ms;
	TArray<USpawnPoint*> GetAttachedSpawnPoints() const;
	TArray<ASprungWheel*> GetSprungWheels() const;
};
