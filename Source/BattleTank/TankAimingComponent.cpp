// Fill out your copyright notice in the Description page of Project Settings.


#include "TankAimingComponent.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#define OUT

// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTankAimingComponent::AimAt(const FVector& HitLocation, const float& LaunchSpeed) const
{
	if(!Barrel)
	{
		UE_LOG(LogTemp, Warning, TEXT("No Barrel to aim from!!!"));
		return;
	}

	FVector LaunchVelocity;
	FVector LaunchDirection;

	if(UGameplayStatics::SuggestProjectileVelocity
	(
		this,
		OUT LaunchVelocity,
		Barrel->GetSocketLocation(FName(TEXT("Projectile"))),
		HitLocation,
		LaunchSpeed,
		false,
		0.f,
		0.f,
		ESuggestProjVelocityTraceOption::DoNotTrace
	))
	{
		LaunchDirection = LaunchVelocity.GetSafeNormal();
		MoveBarrelTowards(LaunchDirection);
		MoveTurretTowards(LaunchDirection);
	};
}

void UTankAimingComponent::SetBarrelReference(UTankBarrel* BarrelToSet)
{
	Barrel = BarrelToSet;
};

void UTankAimingComponent::SetTurretReference(UTankTurret* TurretToSet)
{
	Turret = TurretToSet;
}

void UTankAimingComponent::MoveBarrelTowards(const FVector& LaunchDirection) const
{
	FRotator BarrelRotator = Barrel->GetForwardVector().Rotation();
	FRotator LaunchRotator = LaunchDirection.Rotation();
	FRotator DeltaRotator = LaunchRotator - BarrelRotator;

	Barrel->Elevate(DeltaRotator.Pitch);
}

void UTankAimingComponent::MoveTurretTowards(const FVector& LaunchDirection) const
{
	FRotator LaunchRotation = LaunchDirection.Rotation();
	FRotator TurretRotation = Turret->GetForwardVector().Rotation();
	FRotator DeltaRotation = LaunchRotation - TurretRotation;

	Turret->Rotate(DeltaRotation.Yaw);
}

