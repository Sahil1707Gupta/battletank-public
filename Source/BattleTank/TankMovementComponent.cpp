// Fill out your copyright notice in the Description page of Project Settings.


#include "TankMovementComponent.h"
#include "TankTrack.h"

void UTankMovementComponent::IntendMoveForward(float Throw)
{
    if(LeftTrack && RightTrack)
    {
        LeftTrack->SetThrottle(Throw);
        RightTrack->SetThrottle(Throw);
    };
}

void UTankMovementComponent::IntendMoveRight(float Throw)
{
    if(LeftTrack && RightTrack)
    {
        LeftTrack->SetThrottle(Throw);
        RightTrack->SetThrottle(-Throw);
    }
}

void UTankMovementComponent::Initialise(UTankTrack* LeftTrackToSet, UTankTrack* RightTrackToSet)
{
    if(!LeftTrackToSet || !RightTrackToSet)
    {
        return;
    };

    LeftTrack = LeftTrackToSet;
    RightTrack = RightTrackToSet;
}

void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
    FVector AIForwardIntention = MoveVelocity.GetSafeNormal();
    FVector TankForwardVector = GetOwner()->GetActorForwardVector();

    float ForwardThrow = FVector::DotProduct(TankForwardVector, AIForwardIntention);
    IntendMoveForward(ForwardThrow);

    float RightThrow = FVector::CrossProduct(TankForwardVector, AIForwardIntention).Z;
    IntendMoveRight(RightThrow);
}


