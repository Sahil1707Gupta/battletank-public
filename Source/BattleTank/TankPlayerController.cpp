// Fill out your copyright notice in the Description page of Project Settings.


#include "TankPlayerController.h"
#define OUT

void ATankPlayerController::BeginPlay()
{
    Super::BeginPlay();
    auto ControlledTank = GetControlledTank();
    if(ControlledTank)
    {
        UE_LOG(LogTemp, Warning, TEXT("Name of Controlled Tank : %s"), *GetControlledTank()->GetName());
    };
}

ATank* ATankPlayerController::GetControlledTank() const
{
    return Cast<ATank>(GetPawn());
}

void ATankPlayerController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    AimTowardsCrosshair();
}

void ATankPlayerController::AimTowardsCrosshair() const
{
    if(!GetControlledTank())
    {
        UE_LOG(LogTemp, Error, TEXT("No Controlled tank to aim towards its crosshair..."));
        return;
    };

    FVector HitLocation;

    if(GetSightRayHitLocation(OUT HitLocation))
    {
        GetControlledTank()->AimAt(HitLocation);
    };
}

bool ATankPlayerController::GetSightRayHitLocation(FVector& HitLocation) const
{
    int32 ViewportSizeX, ViewportSizeY;
    GetViewportSize(OUT ViewportSizeX,OUT ViewportSizeY);

    FVector2D CrossHairScreenLocation = FVector2D(ViewportSizeX * CrossHairXLocation, ViewportSizeY * CrossHairYLocation);
    FVector CrossHairWorldDirection;

    GetLookDirection(CrossHairScreenLocation, OUT CrossHairWorldDirection);
    if(GetLookVectorHitLocation(CrossHairWorldDirection, OUT HitLocation))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool ATankPlayerController::GetLookDirection(const FVector2D& CrossHairScreenLocation,FVector& CrossHairWorldDirection) const
{
    FVector CameraWorldLocation;
    return DeprojectScreenPositionToWorld
            (
                CrossHairScreenLocation.X,
                CrossHairScreenLocation.Y,
                OUT CameraWorldLocation,
                OUT CrossHairWorldDirection
            );
}

bool ATankPlayerController::GetLookVectorHitLocation(FVector LookDirection, FVector& HitLocation) const
{
    auto StartLocation = PlayerCameraManager->GetCameraLocation();
    auto EndLocation = StartLocation + (LookDirection * LineTraceRange);
    FHitResult HitResult;
    
    if(GetWorld()->LineTraceSingleByChannel
                    (
                        OUT HitResult,
                        StartLocation,
                        EndLocation,
                        ECollisionChannel::ECC_Visibility
                    ))
    {
        HitLocation = HitResult.Location;
        return true;
    }
    else
    {
        return false;
    }
}